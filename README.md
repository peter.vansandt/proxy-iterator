# proxy-iterator
Header-only library to create arbitrary input and output iterators using only a
lambda. Targeted at C++14 for integration with standard libraries and iterator
based algorithms. MIT license.

## input
Construct an input iterator from a lambda and an index. Alternatively, pass an appropriate lambda to proxy_iterator::begin (or end) to get an input iterator to the beginning of the range. On de-referencing, will call the appropriate lambda with the current index.

Lambda signature such that T is not void:
  using in_lambda=T (*)(size_t);

The lambda must be callable with a single argument of size_t and must not return void.

Note that the input iterator operates on a reference to the lambda. The lambda must live for the duration of the iterator.

## output
Construct an output iterator from a lambda and an index. Alternatively, pass an appropriate lambda and input type to proxy_iterator::begin to get an output iterator to the beginning of the range. On assignment, calls the appropriate lambda with the current index and indicated input type.

Lambda signature:
  using in_lambda=void (*)(size_t, T);

The lambda must be callable with its first argument of size_t and second argument of its input type.

Note that the output iterator operates on a reference to the lambda. The lambda must live for the duration of the iterator.
