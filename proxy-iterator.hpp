// Copyright 2019 Peter Van Sandt
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

namespace proxy_iterator {
template <class T> struct input {
  const T &get;
  size_t index;

  input(const T &f, size_t i) : get(f), index(i) {}
  auto operator*() { return get(index); }
};

template <typename T> struct output {
  const T &put;
  size_t index;
  output(const T &f, size_t i = 0) : put(f), index(i) {}

  template <typename U> output &operator=(const U &x) {
    put(index, x);
    return *this;
  }
  output &operator*() { return *this; }
};

template <class U, class T>
typename std::enable_if<
    std::is_void<typename std::result_of<T(size_t, U)>::type>::value,
    output<T>>::type
begin(T &f) {
  return output<T>(f, 0);
}
template <class T>
typename std::enable_if<
    !std::is_void<typename std::result_of<T(size_t)>::type>::value,
    input<T>>::type
begin(const T &f) {
  return input<T>(f, 0);
}
template <class T> input<T> end(T &f, size_t i) {
  return input<T>(std::forward<T>(f), i);
}

template <typename T> bool operator!=(T &a, T &b) { return a.index != b.index; }
template <typename T> T &operator++(T &a) {
  ++a.index;
  return a;
}
} // namespace proxy_iterator

template <class T> struct std::iterator_traits<proxy_iterator::input<T>> {
  typedef typename std::result_of<T(size_t)>::type value_type;
  typedef input_iterator_tag iterator_category;
};
template <class T> struct std::iterator_traits<proxy_iterator::output<T>> {
  typedef void value_type;
};
