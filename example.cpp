// Copyright 2019 Peter Van Sandt
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <algorithm>
#include <array>
#include <numeric>

int f() {
  std::array<int, 3> a = {1, 2, 3}, b = {1, 2, 3}, c, d;
  auto sum = [&](size_t i) { return a[i] + b[i]; };
  auto fork = [&](size_t i, auto v) {
    c[i] = v;
    d[i] = v;
  };
  std::copy(proxy_iterator::begin(sum), proxy_iterator::end(sum, a.size()),
            proxy_iterator::begin<decltype(c)::value_type>(fork));
  return std::accumulate(std::begin(c), std::end(c), 0);
}
